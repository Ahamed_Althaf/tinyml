Load Machine Learning Model in Embedded Project and Tested on ESP32

Note: This has only been tested with the ESP32.

You should Add .ZIP Library and use one of the included examples as a template for your program.

This repository is a collection of tools and demo projects to help you get started creating your own embedded cooker_whistle_counter system with machine learning. Keyword spotting (or wake word detection) is a form of voice recognition that allows computers (or microcontrollers) to respond to spoken words.

    libs/Speech_recognition - Cooker_Whistle counter TinyML library is added it created by edge impulse
    images/ - I needed to put pictures somewhere
	src/ - main.cpp file for esp32 board 
